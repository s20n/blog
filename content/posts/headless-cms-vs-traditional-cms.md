---
title: "Headless CMS vs Traditional CMS"
author: "Shriram Ravindranathan"
description: "A comparison between headless CMS and traditional CMS solutions"
date: 2022-02-22T17:28:00+05:30
draft: false
tags: [
    "intro",
    "webdev"
]
archives: ["2022", "2022-02"]
---
![Totally Headless Nick](/images/headless.png)

Today is Tue, 22/02/2022. Happy Twosday!

A CMS or Content Management System is a piece of software for digital content management, it can also be used as an enterprise wide digital asset management system, which enables you to manage your digital assets (images, videos, documents and other such files). The CMS comes with a web browser based user interface that allows users to upload content, edit their media files and to control their website.

Headless CMS is a new trend in the CMS world. It is a backend-only solution that provides an API to interact with the content. This type of CMS is often used by developers who want to build their own frontend and don't want to deal with the complexity of the backend frameworks.

In contrast, traditional CMSs are more common for small businesses who need a simple way to manage their website content. They are also easier for non-technical users who just want to update their website without any coding knowledge.

## Why do you need a CMS?

Using a CMS abstracts away a lot of technical intricacies from the content creator.
CMSs are specifically designed so that webmasters or site managers can create, modify and organise digital resources in various ways and for various levels of functionality or to provide search engine optimisation (SEO), online marketing automation processes, online sales opportunities, or self-service tools for their customers.

CMSs have changed the world of web development, it has changed the way in which websites are designed and managed. [Over 15 million domains](https://whatcms.org/Tech_Reports) on the internet run on some CMS technology like Wordpress, Drupal, Joomla, Shopify to name a few.

## Why Headless?

Now, you might be thinking, if it's way easier to use a traditional CMS like wordpress, why on earth would you want to go through the ordeal of decapitating the CMS and having to write your own frontend?

Well, The main benefit of Headless CMS is that it can be used to create an API for your website, which means you can have more control over your data and how it's displayed on your site.

This type of CMS also has many other benefits:

**Performance:**
because there is no frontend interface, the server doesn't have to process as much data and can therefore handle more requests at once. Headless CMSs are also faster than traditional CMSs because they don't need to load all of the content on every page request, which saves time and resources on both server and client side.

**Flexibility:**
You can use any frontend you want, which means developers can choose the best one for their project. This also means that you don't have to worry about updating the frontend when there are changes in the backend.

**Future-proof-ness:**
There is no guarantee that the epic looking theme you chose on wordpress or that wordpress itself will still be in the vogue in the future. Does that mean your content is not valuable anymore? Heck no, your content should not die with the platform and using an API like the ones in Headless CMS will ensure that the content can always be shown wherever you please.


## JAMstack and the rise of Headless

JAM stands for Javascript, API and Markup. It refers to the approach of decoupling the frontend, backend and the database which you would find on a traditional website and distributing it over these three services.

Basically, a Javascript Application retrieves the content from a latent Headless CMS through an API in the form of pre-rendered static markup.
This lets them condense the entire app and serve it over a CDN which leads to blazing fast performance and easily scalable applications.

All this has lead to the rapid adoption of JAMstack applications through platforms like Vercel, Netlify, Github Pages etc. and along with that, brought forth a rise in popularity for Headless CMS platforms.

![JAM Stack](/images/jamstack.png)


## So What's the Catch, Chief?

Well, the main disadvantage of headless CMS is that it requires more technical knowledge than traditional CMS. This means that it might not be suitable for beginners who are just starting out with web development.

![Awsbill.jpeg](/images/Awsbill.jpeg)

Apart from the complexity, there's a cost factor too. You know that wordpress hosting is quite affordable nowadays and the websites who provide really good offers on shared hosting are a dime a dozen. However, you're going to have to shell out a little bit more if you are going to opt for a headless CMS solution like Strapi. Not to mention the cost of development if you are not really well versed with the tooling.

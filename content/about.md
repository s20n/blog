+++
title = "About"
description = "My blog"
date = "2023-06-30"
aliases = ["about-me", "contact"]
author = "Shriram Ravindranathan"
+++

Hi I am Shriram. I mostly work with C++. Here are [some of my pet projects](https://github.com/shriramters).

I like making software, music and music software. Please check out my project [chordcat](https://github.com/shriramters/chordcat) if you are into making music as well.

I also maintain [a few packages on debian](https://qa.debian.org/developer.php?login=s20n%40ters.dev). 

PGP Key: [93DA218D825BD263300F7BD6FC7E951A7BEF0836](https://keys.openpgp.org/vks/v1/by-fingerprint/93DA218D825BD263300F7BD6FC7E951A7BEF0836)

